SG_NAME=yc-security-group
SG_ID=$(yc vpc security-group get --name $SG_NAME --format json | jq .id | tr -d '"')
SSH_KEY=$(cat ~/.ssh/id_rsa.pub)
USER_NAME=mike
ZONE=ru-central1-b

yc managed-kubernetes cluster create \
  --name=kube-infra \
  --public-ip \
  --network-name=default \
  --service-account-name=kube-infra \
  --node-service-account-name=kube-infra \
  --release-channel=rapid \
  --zone=$ZONE \
  --security-group-ids=$SG_ID \
  --folder-name default

yc managed-kubernetes node-group create \
  --name=group-1 \
  --cluster-name=kube-infra \
  --cores=2 \
  --memory=4G \
  --preemptible \
  --auto-scale=initial=1,min=1,max=2 \
  --network-interface=subnets=default-ru-central1-b,ipv4-address=nat,security-group-ids=$SG_ID \
  --folder-name default \
  --metadata="ssh-keys=${USER_NAME}:${SSH_KEY}"

kubectl config unset contexts.yc-kube-infra
yc managed-kubernetes cluster get-credentials --name=kube-infra --external
