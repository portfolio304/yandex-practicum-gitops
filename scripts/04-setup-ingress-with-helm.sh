export HELM_EXPERIMENTAL_OCI=1
cat sa-key.json | helm registry login cr.yandex --username 'json_key' --password-stdin

# Скачиваем чарт Ingress Controller в папку charts:
helm pull oci://cr.yandex/yc-marketplace/yc-alb-ingress-controller-chart \
--version=v0.1.3 \
--untar \
--untardir=charts


# Устанавливаем чарт в кластер
export FOLDER_ID=$(yc config get folder-id)
export CLUSTER_ID=$(yc managed-kubernetes cluster get kube-infra | head -n 1 | awk -F ': ' '{print $2}')

# helm secrets upgrade -i \
# --create-namespace \
# --namespace yc-alb-ingress \
# --set folderId=$FOLDER_ID \
# --set clusterId=$CLUSTER_ID \
# --set-file saKeySecretKey=sa-key.json \
# yc-alb-ingress-controller ./charts/yc-alb-ingress-controller-chart/

helm secrets upgrade -i \
--create-namespace \
--namespace yc-alb-ingress \
--values values/alb.yaml \
yc-alb-ingress-controller ./charts/yc-alb-ingress-controller-chart

# helm upgrade -i \
# --create-namespace \
# --namespace yc-alb-ingress \
# --values values/alb.yaml.dec \
# yc-alb-ingress-controller ./charts/yc-alb-ingress-controller-chart
# проверяем, что ресурсы создались
kubectl -n yc-alb-ingress get all