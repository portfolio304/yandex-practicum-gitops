SG_NAME=yc-security-group
DOMAIN=ya-practice.devopsim.ru

SUBNET_ID=$(yc vpc subnet get default-ru-central1-b --format json | jq .id | tr -d '"')
EXTERNAL_IP=$(yc vpc address get --name infra-alb --format json | jq .external_ipv4_address.address | tr -d '"')
SG_ID=$(yc vpc security-group get --name $SG_NAME --format json | jq .id | tr -d '"')
CERT_ID=$(yc certificate-manager certificate get --name kube-infra --format json | jq .id | tr -d '"')


cat << EOF > man.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: httpbin
  labels:
    app: httpbin
spec:
  replicas: 1
  selector:
    matchLabels:
      app: httpbin
  template:
    metadata:
      labels:
        app: httpbin
    spec:
      containers:
        - name: httpbin
          image: kennethreitz/httpbin:latest
          ports:
            - name: http
              containerPort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: httpbin
spec:
  type: NodePort
  selector:
    app: httpbin
  ports:
    - name: http
      port: 80
      targetPort: 80
      protocol: TCP
      nodePort: 30081
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: httpbin
  annotations:
    ingress.alb.yc.io/subnets: $SUBNET_ID
    ingress.alb.yc.io/external-ipv4-address: $EXTERNAL_IP
    ingress.alb.yc.io/group-name: infra-alb
    ingress.alb.yc.io/security-groups: $SG_ID
spec:
  tls:
    - hosts:
        - "httpbin.infra.$DOMAIN"
      secretName: yc-certmgr-cert-id-$CERT_ID
  rules:
    - host: "httpbin.infra.$DOMAIN"
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: httpbin
                port:
                  number: 80
EOF