echo Create values files

cat <<EOF > values/cluster-prod.yaml
clusterName: "prod"
clusterVersion: "1.21"
clusterZone: "ru-central1-b"
# не забудьте, что эти диапазоны адресов должны
# быть указаны в SecurityGroup prod, созданной ранее
clusterIpv4Range: "10.113.0.0/16"  
serviceIpv4Range: "10.97.0.0/16"
nodeCount: 1
nodeDiskSize: 64
nodeMem: 2
nodeCores: 2
securityGroup: prod-security-group
nodeSubnet: default-ru-central1-b
EOF

echo Add cluster to ArgoCD app

cat <<EOF > charts/apps/templates/cluster-prod.yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: cluster-prod
  # Обратите внимание на namespace – он должен быть argocd
  namespace: argocd
spec:
  destination:
    # А вот тут уже неймспейс, куда будет устанавливаться сам чарт
    namespace: infrastructure
    server: {{ .Values.spec.destination.server }}
  project: default
  source:
    # Указываем путь к чарту
    path: charts/k8s-cluster
    repoURL: {{ .Values.spec.source.repoURL }}
    targetRevision: {{ .Values.spec.source.targetRevision }}
    helm:
      valueFiles:
        - ../../values/cluster-prod.yaml
  syncPolicy:
    automated: {}
EOF

echo Done