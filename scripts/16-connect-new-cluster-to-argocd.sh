# скачиваем конфиг для нового кластера
ARGOCD_SERVER=argocd.infra.ya-practice.devopsim.ru

yc managed-kubernetes cluster get-credentials --name=kube-prod --external
argocd login <адрес argocd>:443  # далее вводим логин admin и его пароль
# добавляем кластер в argocd
argocd --grpc-web cluster add yc-kube-prod