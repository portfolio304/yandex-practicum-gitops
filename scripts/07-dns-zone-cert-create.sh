DOMAIN=yp.devopsim.ru #ya-practice.devopsim.ru
ZONE_NAME=yp #yc-courses
CERT_NAME=yp-devopsim-ru #kube-infra

echo Create record zone:

yc dns zone create \
--name $ZONE_NAME --zone $DOMAIN. \
--public-visibility 

echo zone created

echo Create wildcard A-type:
# Создадим wildcard A-запись, указывающую на IP балансировщика:
LB_IP=$(yc vpc address get --name infra-alb --format json | jq .external_ipv4_address.address | tr -d '"')

yc dns zone add-records --name $ZONE_NAME --record "*.infra.$DOMAIN. 600 A $LB_IP"

echo "record *.infra.$DOMAIN. created"

# cертификат на доменное имя, которое использовали ранее для приложения httpbin:
yc certificate-manager certificate request \
  --name $CERT_NAME \
  --domains "*.infra.$DOMAIN" \
  --challenge dns 


CERT_ID=$(yc certificate-manager certificate get --name $CERT_NAME --format json | jq .id | tr -d '"')
# Для автоматической проверки владения доменом нам потребуется создать специальную CNAME-запись, ведущую на certificate-manager:
yc dns zone add-records --name $ZONE_NAME --record \
"_acme-challenge.infra.$DOMAIN. 600 CNAME $CERT_ID.cm.yandexcloud.net." 