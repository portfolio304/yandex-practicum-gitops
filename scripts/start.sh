LB_ID=$(yc application-load-balancer load-balancer list --format json | jq '.[0].id' | tr -d '"')

echo Starting kube-infra
yc managed-kubernetes cluster start kube-infra &

echo Starting kube-prod
yc managed-kubernetes cluster start kube-prod &

echo starting load_balanser with ID $LB_ID
yc application-load-balancer load-balancer start $LB_ID
