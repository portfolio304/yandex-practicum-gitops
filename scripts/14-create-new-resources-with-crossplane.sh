
echo Создаём новую группу безопасности через Crossplane:

cat << EOF > charts/apps/templates/security-groups.yaml
kind: SecurityGroup
apiVersion: vpc.yandex-cloud.jet.crossplane.io/v1alpha1
metadata:
  name: prod-security-group
spec:
  forProvider:
    name: prod
    folderIdRef:
      name: default
    networkIdRef:
      name: default
    ingress:
      - description: rule-https
        protocol: tcp
        port: 443
        v4CidrBlocks:
          - 0.0.0.0/0
      - description: rule-internal-traffic
        protocol: any
        fromPort: 0
        toPort: 65535
        predefinedTarget: self_security_group
      - description: rule-internal-k8s-pods-services
        protocol: any
        fromPort: 0
        toPort: 65535
        v4CidrBlocks:
          - 10.97.0.0/16  # новый диапазон для адресов подов будущего кластера
          - 10.113.0.0/16  # новый диапазон для адресов сервисов будущего кластера
      - description: rule-yc-balancing
        protocol: tcp
        fromPort: 0
        toPort: 65535
        v4CidrBlocks:
          - 198.18.235.0/24
          - 198.18.248.0/24
      - description: rule-icmp-internal
        protocol: icmp
        v4CidrBlocks:
          - 10.0.0.0/8
          - 192.168.0.0/16
          - 172.16.0.0/12
    egress:
      - description: rule-egress-all
        protocol: any
        fromPort: 0
        toPort: 65535
        v4CidrBlocks:
          - 0.0.0.0/0
EOF

echo Создаём новый кластер для приложений

echo Создаем чарт
mkdir -p charts/k8s-cluster/templates

cat <<EOF > charts/k8s-cluster/Chart.yaml
apiVersion: v2
name: k8s-cluster
description: k8s-cluster in yc cloud
version: 0.1.0
appVersion: "1.0"
EOF

echo Create helpers template

cat <<EOF > charts/k8s-cluster/templates/_helpers.tpl
{{/*
clusterFullName for service accounts/cluster/nodegroup
*/}}
{{- define "clusterFullName" -}}
kube-{{ required "clusterName must be specified" .Values.clusterName }}
{{- end -}}
EOF

echo Create service account template

cat <<EOF > charts/k8s-cluster/templates/service-account.yaml
apiVersion: iam.yandex-cloud.jet.crossplane.io/v1alpha1
kind: ServiceAccount
metadata:
  name: {{ include "clusterFullName" . }}
spec:
  forProvider:
    name: {{ include "clusterFullName" . }}
    folderIdRef:
      name: default
---
apiVersion: iam.yandex-cloud.jet.crossplane.io/v1alpha1
kind: FolderIAMMember
metadata:
  name: {{ include "clusterFullName" . }}-editor
spec:
  forProvider:
    role: editor
    serviceAccountRef:
      name: {{ include "clusterFullName" . }}
    folderIdRef:
      name: default
EOF

echo Create cluster template
cat <<EOF > charts/k8s-cluster/templates/cluster.yaml
apiVersion: kubernetes.yandex-cloud.jet.crossplane.io/v1alpha1
kind: Cluster
metadata:
  name: {{ include "clusterFullName" . }}
spec:
  forProvider:
    name: {{ include "clusterFullName" . }}
    master:
      - zonal:
          - zone: {{ .Values.clusterZone }}
        publicIp: true
        version: "{{ .Values.clusterVersion }}"
        securityGroupIdsRefs:
          - name: {{ .Values.securityGroup }}
    nodeServiceAccountIdRef:
      name: {{ include "clusterFullName" . }}
    networkIdRef:
      name: default
    serviceAccountIdRef:
      name: {{ include "clusterFullName" . }}
    folderIdRef:
      name: default
    releaseChannel: RAPID
    clusterIpv4Range: {{ .Values.clusterIpv4Range }}
    serviceIpv4Range: {{ .Values.serviceIpv4Range }}
EOF

echo Create nodegroup template
cat <<EOF > charts/k8s-cluster/templates/node-group.yaml
apiVersion: kubernetes.yandex-cloud.jet.crossplane.io/v1alpha1
kind: NodeGroup
metadata:
  name: {{ include "clusterFullName" . }}-nodegroup-1
spec:
  forProvider:
    clusterIdRef:
      name: {{ include "clusterFullName" . }}
    name: {{ include "clusterFullName" . }}-nodegroup-1
    version: "{{ .Values.clusterVersion }}"
    instanceTemplate:
      - platformId: "standard-v2"
        networkInterface:
          - securityGroupIdsRefs:
              - name: {{ .Values.securityGroup }}
            nat: true
            subnetIdsRefs:
              - name: {{ .Values.nodeSubnet }}
        resources:
          - memory: {{ .Values.nodeMem }}
            cores: {{ .Values.nodeCores }}
        bootDisk:
          - type: "network-hdd"
            size: {{ .Values.nodeDiskSize }}
        schedulingPolicy:
          - preemptible: true
    scalePolicy:
      - fixedScale:
          - size: {{ .Values.nodeCount }}
    allocationPolicy:
      - location:
          - zone: {{ .Values.clusterZone }}
    maintenancePolicy:
      - autoUpgrade: true
        autoRepair: true
        maintenanceWindow:
          - startTime: "23:00"
            duration: "3h"
            day: "saturday"
EOF

echo Create values file
cat <<EOF > charts/k8s-cluster/values.yaml
clusterName:
clusterVersion: "1.21"
clusterZone: "ru-central1-b"
clusterIpv4Range:
serviceIpv4Range:
nodeCount: 1
nodeDiskSize: 64
nodeMem: 2
nodeCores: 2
securityGroup:
nodeSubnet:
EOF

echo Done