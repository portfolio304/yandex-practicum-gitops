echo Create app of apps

CHART_DIR=charts/apps-prod
GITLAB_REPO_URL=https://gitlab.com/portfolio304/yandex-practicum-gitops.git
K8S_ADDRESS=https://158.160.28.45

CHART_TEMPLATES_DIR=$CHART_DIR/templates

mkdir -p $CHART_TEMPLATES_DIR

cat <<EOF > $CHART_DIR/Chart.yaml
apiVersion: v2
name: applications-prod
description: Applications for prod cluster
version: 0.1.0
appVersion: "1.0"
EOF

cat <<EOF > $CHART_DIR/values.yaml
spec:
  source:
    repoURL:
    targetRevision:
  destination:
    server:
EOF

cat <<EOF > values/apps-prod.yaml
spec:
  source:
    repoURL: $GITLAB_REPO_URL
    targetRevision: HEAD
  destination:
    server: $K8S_ADDRESS
EOF

echo Add new app to argocd values: server.additionalApplications
echo
cat <<EOF
- name: apps-prod
  namespace: argocd
  project: default
  source:
    helm:
      valueFiles:
      - ../../values/apps-prod.yaml
    path: charts/apps-prod
    repoURL: $GITLAB_REPO_URL
  destination:
    namespace: argocd
    server: https://kubernetes.default.svc
  syncPolicy:
    automated: {}
EOF