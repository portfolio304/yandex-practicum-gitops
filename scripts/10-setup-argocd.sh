export HELM_EXPERIMENTAL_OCI=1
helm pull oci://cr.yandex/yc-marketplace/yandex-cloud/argo/chart/argo-cd \
--untar --untardir=charts --version=4.5.3-1 

kubectl create namespace argocd

kubectl -n argocd create secret generic \
helm-secrets-private-keys --from-file=key.txt 

helm secrets upgrade -i -n argocd \
  argocd charts/argo-cd \
  --values values/argocd.yaml \
