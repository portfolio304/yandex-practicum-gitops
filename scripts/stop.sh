LB_ID=$(yc application-load-balancer load-balancer list --format json | jq '.[0].id' | tr -d '"')
echo $LB_ID
echo stoping kube-infra

yc managed-kubernetes cluster stop kube-infra &

echo stoping kube-prod
yc managed-kubernetes cluster stop kube-prod &

echo stoping load-balancer $LB_ID
yc application-load-balancer load-balancer stop $LB_ID &

echo Done