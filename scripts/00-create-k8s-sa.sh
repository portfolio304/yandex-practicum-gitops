yc iam service-account create kube-infra
# назначаем сервисному аккаунту роль editor
yc resource-manager folder add-access-binding \
  --name=default \
  --service-account-name=kube-infra \
  --role=editor 