#!/bin/bash

kubectl create namespace gitlab

helm secrets upgrade --install \
--namespace gitlab gitlab-runner gitlab/gitlab-runner \
-f values/gitlab-runner.yaml