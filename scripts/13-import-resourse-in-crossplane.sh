YC_FOLDER_ID=b1ghnr59h7djbhc3k8f7
NETWORK_ID=$(yc vpc network get default --format json | jq .id | tr -d '"')
SUBNET_ID=$(yc vpc subnet get default-ru-central1-b --format json | jq .id | tr -d '"')


mkdir -p charts/infra-yc/templates

cat <<EOF > charts/infra-yc/templates/import.yaml

# Первым делом импортируем каталог
apiVersion: resourcemanager.yandex-cloud.jet.crossplane.io/v1alpha1
kind: Folder
metadata:
  name: default
  annotations:
    crossplane.io/external-name: $YC_FOLDER_ID
spec:
  # этот параметр для безопасности
  # чтобы не удалить каталог при удалении ресурса из kubernetes
  deletionPolicy: Orphan
  forProvider:
    # default – стандартное название каталога по умолчанию
    # если ваш каталог называется по-другому – указываем это другое имя
    name: default
---
# Следующим шагом импортируем сеть
kind: Network
apiVersion: vpc.yandex-cloud.jet.crossplane.io/v1alpha1
metadata:
  name: default
  annotations:
    crossplane.io/external-name: $NETWORK_ID
spec:
  deletionPolicy: Orphan
  forProvider:
    name: default
    # обратите внимание, что id каталога уже указывать не надо
    # достаточно использовать референс на другую сущность crossplane
    folderIdRef:
      name: default
---
# Импортируем подсеть default-ru-central1-b
kind: Subnet
apiVersion: vpc.yandex-cloud.jet.crossplane.io/v1alpha1
metadata:
  name: default-ru-central1-b
  annotations:
    crossplane.io/external-name: $SUBNET_ID
spec:
  forProvider:
    networkIdRef:
      name: default
    folderIdRef:
      name: default
    v4CidrBlocks:
      - 10.129.0.0/24  # берем из yc vpc subnet get default-ru-central1-b
EOF



cat <<EOF > charts/infra-yc/Chart.yaml 
apiVersion: v2
name: infra-yc
description: infra-yc
version: 0.1.0
appVersion: "1.0"
EOF

cat <<EOF > charts/infra-yc/values.yaml
EOF

cat <<EOF > charts/apps/templates/infra-yc.yaml
# Создаем неймспейс для инфраструктуры
apiVersion: v1
kind: Namespace
metadata:
  name: infrastructure
  annotations:
    # С помощью этого параметра указываем порядок создания
    argocd.argoproj.io/sync-wave: "-1"
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: infra-yc
  # Обратите внимание на namespace – он должен быть argocd
  namespace: argocd
spec:
  destination:
    # А вот тут уже неймспейс, куда будет устанавливаться сам чарт
    namespace: infrastructure
    server: {{ .Values.spec.destination.server }}
  project: default
  source:
    # Указываем путь к чарту
    path: charts/infra-yc
    repoURL: {{ .Values.spec.source.repoURL }}
    targetRevision: {{ .Values.spec.source.targetRevision }}
  syncPolicy:
    automated: {}
EOF