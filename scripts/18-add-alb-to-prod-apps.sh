FOLDER_ID=$(yc config get folder-id)
CLUSTER_ID=$(yc managed-kubernetes cluster get kube-prod | head -n 1 | awk -F ': ' '{print $2}')
KEY_FILE=sa-key-2.json

cat <<EOF > values/alb-prod.yaml
folderId: $FOLDER_ID
clusterId: $CLUSTER_ID
saKeySecretKey: "$(cat $KEY_FILE  | jq -r tostring)"
  
EOF

# helm secrets enc values/alb-prod.yaml

cat <<EOF > charts/apps-prod/templates/alb.yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: yc-alb-ingress-prod
  namespace: argocd
spec:
  destination:
    namespace: yc-alb-ingress
    server: {{ .Values.spec.destination.server }}
  project: default
  source:
    path: charts/yc-alb-ingress-controller-chart
    repoURL: {{ .Values.spec.source.repoURL }}
    targetRevision: {{ .Values.spec.source.targetRevision }}
    helm:
      valueFiles:
        - secrets+age-import:///helm-secrets-private-keys/key.txt?../../values/alb-prod.yaml
  syncPolicy:
    automated: {}
    syncOptions:
      - CreateNamespace=true
EOF