export HELM_EXPERIMENTAL_OCI=1
helm pull oci://cr.yandex/yc-marketplace/yandex-cloud/argo/chart/argo-cd \
--untar --untardir=charts --version=4.5.3-1

helm install -n argocd \
  --create-namespace \
  argocd charts/argo-cd