yc iam service-account create --name ingress-controller

# Роль alb.editor нужна для создания балансировщиков
yc resource-manager folder add-access-binding default \
--service-account-name=ingress-controller \
--role alb.editor

# Роль vpc.publicAdmin нужна для управления внешними адресами
yc resource-manager folder add-access-binding default \
--service-account-name=ingress-controller \
--role vpc.publicAdmin

# Роль certificate-manager.certificates.downloader
# нужна для скачивания сертификатов из Yandex Certificate Manager
yc resource-manager folder add-access-binding default \
--service-account-name=ingress-controller \
--role certificate-manager.certificates.downloader

# Роль compute.viewer нужна для добавления нод в балансировщик
yc resource-manager folder add-access-binding default \
--service-account-name=ingress-controller \
--role compute.viewer 